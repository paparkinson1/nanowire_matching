%% Show quad
%   Author : Patrick Parkinson
%   Date   : 19th April 2018
%   Email  : patrick.parkinson@manchester.ac.uk
%   License: CC BY-SA 4.0
%
%   show_quad(cent,ori,c)
%       cent    :   Center positions of quad
%       ori     :   orientation of nanowires (if relevant)
%       c       :   color to plot
%
%   A helper function to show a given quad.
function show_quad(cent,ori,c)
if nargin < 2
    ori = zeros(numel(cent)/2,1);
    noori = true;
else
    noori = false;
end
if isempty(ori)
    noori=true;
end
hold on;
cent=reshape(cent,2,numel(cent)/2)';
b   = boundary(cent);
if noori
    plot(cent(b,1),cent(b,2),'ko');
else
    quiver(cent(b,1),cent(b,2),sin(ori(b)'),cos(ori(b)'),1);
end
plot(cent(b,1),cent(b,2),'-','color',c);
end