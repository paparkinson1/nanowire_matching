%% Show best fitting transformation
%   Author : Patrick Parkinson
%   Date   : 19th April 2018
%   Email  : patrick.parkinson@manchester.ac.uk
%   License: CC BY-SA 4.0
%
%   tr=show_transformation(sample_points,reference_points, scores, affines,settings)
%       tr                  :   best transformation
%       sample_points       :   2D array of sample points
%       reference_points    :   2D array of reference points
%       scores              :   score array
%       affines             :   affine/match array
%       settings            :   settings structure
%
%   show_transformation produces a figure showing all data points along
%   with match positions (with sizes proportional to their score). This
%   predominantly internal function is a helper to allow visual inspection
%   of matches and overlaps.
function tr=show_transformation(sample_points,reference_points, scores, affines,settings)

if nargin<5
    settings=[];
    warning('No settings provided. Default settings used.');
end

% Hardcoded settings
settings = check_settings(settings,'resolution',200e-6);
settings = check_settings(settings,'sample_radius',6.5);
settings = check_settings(settings,'cutoff',min(scores)-1);
settings = check_settings(settings,'match_radius',15);

% Clip
trans = affines(scores>settings.cutoff);
sout  = scores(scores>settings.cutoff);

% Find best scoring transformation
[~,ii] = max(sout);
tr    = trans(ii);

% Apply transformation to sample (remember)
tx = transformPointsForward(tr, double(sample_points'))'; 

% Separate affines
o=zeros(numel(trans),2);
s=zeros(numel(trans),1);
t=zeros(numel(trans),1);
for i=1:numel(trans)
    [s(i),t(i),o(i,:)] = sto_affine(trans(i));
end

% Find matched points
dd        = pdist2(tx',reference_points');
dd        = any(dd'<settings.match_radius);
matched   = tx(:,dd);
unmatched = tx(:,~dd);

%% Show
clf;
subplot(121);
[so, i] = sort(sout,'descend');
scatter(o(i,1)*settings.resolution,o(i,2)*settings.resolution,(so-min(so)+1)*(500/max(so)),so,'filled');
axis('image');
c=colorbar;
ylabel(c,'Hypothesis score');
th = linspace(0,2*pi);
hold on;
scatter(reference_points(1,:)*settings.resolution,reference_points(2,:)*settings.resolution,1,'.','k');
scatter(tx(1,:)*settings.resolution,tx(2,:)*settings.resolution,5,'o','filled','r');
plot(settings.sample_radius*sin(th),settings.sample_radius*cos(th),'k--');
hold off;
title('Sample overview with quad codepoint hits');

subplot(122);
axis('image');
hold on;
scatter(matched(1,:)*settings.resolution,matched(2,:)*settings.resolution,40,'d','filled','g');
scatter(unmatched(1,:)*settings.resolution,unmatched(2,:)*settings.resolution,20,'x','r');
scatter(reference_points(1,:)*settings.resolution,reference_points(2,:)*settings.resolution,20,'o','filled','k');
hold off;
box on;
xlim([min(tx(1,:)), max(tx(1,:))]*settings.resolution);
ylim([min(tx(2,:)),max(tx(2,:))]*settings.resolution);
legend('Matched','Unmatched','Reference','location','best');
end

%% Helper function for settings
function s = check_settings(s,field,default)
    if ~isfield(s,field)
        s.(field) = default;
    end
end