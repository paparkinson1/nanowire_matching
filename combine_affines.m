%% Combine affines
%   Author : Patrick Parkinson
%   Date   : 19th July 2017
%   Email  : patrick.parkinson@manchester.ac.uk
%   
function [out_affines,out_scores]=combine_affines(affines,scores,settings)

settings = check_settings(settings,'combine_distance',15);
settings = check_settings(settings,'show_info',false);

if numel(affines) == 1
    out_affines = affines;
    out_scores  = scores;
    return;
end

ofs = zeros(numel(affines),2);
for i =1:numel(affines)
    [~,~,ofs(i,:)] = sto_affine(affines(i));
end
offset_distances = squareform(pdist(ofs))<settings.combine_distance;

out_affines = affine2d;
out_scores  = [];
k=0;
for i =1:numel(affines)
    f = find(offset_distances(i,:));
    offset_distances(:,f) = 0;
    o = zeros(3,3);
    if numel(f)>0
        k=k+1;
        for j =1:numel(f)
            o = o + affines(f(j)).T*max(1e-8,scores(f(j)));
        end
        o = o./max(1e-8,sum(scores(f)));
        out_affines(k) = affine2d(o); 
        out_scores(k)  = sum(scores(f)); %#ok<AGROW>
    end
end
if settings.show_info
    disp(['Combined ',int2str(numel(affines)), ' to ',int2str(k), ' affines']);  
end
end


function s = check_settings(s,field,default)
    if ~isfield(s,field)
        s.(field) = default;
    end
end