%% Generate quad hash codes from provided position array
%   Author      : Patrick Parkinson
%   Institution : School of Physics & Astronomy, University of Manchester
%   V1 Date     : 19th April 2017
%   Date        : 19th April 2018
%   Email       : patrick.parkinson@manchester.ac.uk
%   License     : CC BY-SA 4.0
%
%   [hashs,cents, hash_tree]=quads(pos_array,settings)
%       hashs       :   output hash array
%       cents       :   output hash centre array
%       hash_tree   :   output hash_tree (suitable for kd-tree searching)
%       pos_array   :   input position array of points
%       settings    :   input settings structure
%
%   Quads is the main function to generate quad hashs from a point array,
%   with performance controlled via the settings structure.
%   
function [hashs,cents, hash_tree]=quads(pos_array,settings)
% Check runtime settings provided
if nargin<2
    settings = [];
    warning('No run-time settings provided, using defaults');
end
if nargin < 1
    error('No input arguaments provided. Exiting.');
end

% Flags
settings = check_settings(settings,'debug_text',false);
settings = check_settings(settings,'debug_image',false);
settings = check_settings(settings,'show_info',true);
settings = check_settings(settings,'pack32',false);             % Pack into 32 bit integers

% Settings
settings = check_settings(settings,'resolution',200e-6);        % Minimum resolution
settings = check_settings(settings,'nw_per_grid',40);           % Target nws per grid
settings = check_settings(settings,'minimum_quad_size',200);    % Min size (in resl)
settings = check_settings(settings,'maximum_quad_size',2000);   % Max size (in resl)
settings = check_settings(settings,'repeats_per_grid',70);      % Attempts per grid area
settings = check_settings(settings,'step_grid',0.4);            % Stepping unit
settings = check_settings(settings,'bits',0);                   % Interger type to use
settings = check_settings(settings,'allocate_space',1e6);       % Interger type to use
settings = check_settings(settings,'randomize',false);          % Enable ran

% Parallel processing [not implemented]
settings.workers = numlabs;

% Helpers
if settings.bits == 16
    int        = @int16;
elseif settings.bits == 32
    int        = @int32;
elseif settings.bits == 8
    int        = @int8;
elseif settings.bits == 0
    int        = @double;
end
typeint = int(0);

if settings.show_info; disp('--> Generate Quads'); end

%% Randomize

if settings.randomize
    rng('shuffle');
else
    rng(0);
end

%% Prepare matrix

s = size(pos_array);
s = s(2);
if settings.show_info
    disp(['Using ',int2str(s),' points on ',num2str(settings.resolution*1e3),'um resolution']);
end

%% Calculate grid size

ma            = double(max(pos_array,[],2));    % Find extent of position array
mi            = double(min(pos_array,[],2));
points_centre = double(ma-mi)/2+mi;             % Find geometrical centre
si            = prod(ma - mi);                  % Total size in resolution units^2
nw_typ_area   = (si/s);                         % Typical nanowire area (for choosing grid size)
grid_size     = int(sqrt(nw_typ_area*settings.nw_per_grid));
num_grids     = ceil(double(ma-mi)./double(grid_size));

if settings.show_info
    disp(['Quad grid size=',int2str(double(grid_size)*settings.resolution*1e3),'um. Number of grids=',int2str(num_grids(1)),' x ',int2str(num_grids(2))]);
end

%% Iterate over grids
% Helpers
mat     = [1 2 3 4;1 3 2 4;1 4 2 3;2 3 1 4;2 4 1 3; 3 4 1 2];

% Counters
quads   = 0;
lquads  = 0;
dots    = 0;

% Containers
hashs   = zeros(settings.allocate_space,4,'uint8');         % Initialise with 1M hash spaces
cents   = zeros(settings.allocate_space,8,'like',typeint);  % Same for nw coordinates

% Iterate over all grids
for ii = (-num_grids(1):settings.step_grid:num_grids(1))/2
    for jj = (-num_grids(2):settings.step_grid:num_grids(2))/2
        r = [ii,jj];
        %  Pick out relevant nanowires in this grid
        sub_mask = (pos_array(1,:) > r(1)*grid_size+points_centre(1)).*...
            (pos_array(1,:) < (r(1)+1)*grid_size+points_centre(1)).*...
            (pos_array(2,:) > r(2)*grid_size+points_centre(2)).*...
            (pos_array(2,:) < (r(2)+1)*grid_size+points_centre(2));
        
        % Check if more than 4 nanowires
        if sum(sub_mask)>4
            sub = pos_array(:,logical(sub_mask));
        else
            if settings.debug_text; disp('XXX -> Too few points');end
            continue;
        end
        
        % For each grid, try and make up to "repeats_per_grid" quads
        for kk = 1:min(factorial(sum(sub_mask))/factorial(4),settings.repeats_per_grid)
            [hash,cent]=generate_quad(sub);     % MAIN FUNCTION %
            if numel(hash)>0
                quads = quads+1;
                hashs(quads,:) = hash;
                cents(quads,:) = cent;
                if and(settings.show_info,quads-lquads > 1000)
                    fprintf(1,'.');dots =dots+1;lquads=quads;
                    if (dots>0) && (mod(dots,40) == 0);fprintf(1,'\n');end
                end
            end
        end
    end
end

% Clip down and return
hashs = hashs(1:quads,:);
cents = cents(1:quads,:);

if settings.pack32 == true
    % Pack 4 bytes to a 32 bit number
    h32    = uint32(hashs(:,1)) +2^8*uint32(hashs(:,2)) +2^16*uint32(hashs(:,3)) +2^24*uint32(hashs(:,4));
    % Remove hash-code clashes
    [hashs,ia] = unique(h32);
else
    % Do not pack, simply remove clashes
    [hashs,ia] = unique(hashs,'rows');
end
% Clip down cents
cents      = cents(ia,:);

% Create a KD tree if requested
if nargout == 3
    if  settings.pack32==false
        hash_tree = createns(double(hashs));
    else
        warning('Must disable packing to output a KD tree');
    end
end

if settings.show_info
    disp(' ');
    disp(['Found ',int2str(length(hashs)),' (',int2str(quads-length(hashs)),' discarded)']);
end

%%%%%%%%%%%%% Generate Quad sub-function %%%%%%%%%%%%%
    function [hash,cent]=generate_quad(sub)
        % How many attempts to make before giving up
        fail  = 0;
        si    = size(sub);
        while fail<10
            fail=fail+1;
            % Choose 4 points
            r_n   = randperm(si(2),4);
            sub_q = sub(:,r_n);
            
            % Make a first guess quad, by locating extrema to be a and b
            v       = double(sub_q');
            v1      = v([1 1 1 2 2 3],:);
            v2      = v([2 3 4 3 4 4],:);
            dist    = sum((v2-v1).^2,2);
            [di,e]  = max(dist);
            di      = sqrt(di);
            % Check for minimum size
            if or(di < settings.minimum_quad_size,di>settings.maximum_quad_size)
                if settings.debug_text; disp('XXX -> Not max/min size constraint');end
                continue;
            end
            
            % Assign nw positions to a,b,c,d
            a = sub_q(:,mat(e,1));
            b = sub_q(:,mat(e,2));
            c = sub_q(:,mat(e,3));
            d = sub_q(:,mat(e,4));
            
            % Find center
            if settings.bits == 0
                cent = (a+b)/2;
            else
                cent = bitshift(a,-1)+bitshift(b,-1);
            end
            rad = di/2;
            rad2=rad^2;
            
            % Check if within circle
            if (sum(double(c-cent).^2)>rad2) || (sum(double(d-cent).^2)>rad2)
                if settings.debug_text; disp('XXX -> Not within circle');end
                continue;
            end
            
            % Convert co-ordinate system to quad co-ordinates
            p = [0 1;0 1];              % a and b
            ax= double(b-a);
            th= pi/4-atan(ax(1)/ax(2));
            % Strange fudge required to rotate data points in certain
            % circumstances
            if sign(ax(2))==-1
                th =th + pi;
            end
            
            % Rotate nanowire set to quad system
            rmx = [cos(-th),-sin(-th);sin(-th) cos(-th)];
            pc= rmx*(double((c-a)))/(sqrt(2)*rad);
            pd= rmx*(double((d-a)))/(sqrt(2)*rad);
            
            % Enforce rules [1] - x_c + x_d to sum to less than 1
            if (pc(1)+pd(1)) > 1
                % Switch a and b - equivalent 180 rotation plus offset
                rmp = [-1 0;0 -1];
                pc = rmp*pc+[1 1]';
                pd = rmp*pd+[1 1]';
                if settings.debug_text;disp('XXX -> switch a and b');end
                t = a;a = b;b = t;
            end
            % Enforce rules [2] - x_c to be smaller than x_d
            if pc(1) > pd(1)
                % Switch c and d
                t  = pc;    pc = pd;    pd = t;
                if settings.debug_text;disp('XXX -> switch c and d');end
                t = c;  c = d;  d = t;
            end
            
            % Complete the hash
            p(:,3) = pc;
            p(:,4) = pd;
            
            %  Create hash
            if any(any(p<0))
                if settings.debug_text; disp('XXX -> cannot hash - negative');end
                continue;
            else
                hash=uint8([p(:,3)*256;p(:,4)*256]');
            end
            fail = 20;  % End loop
        end
        if fail == 10
            if settings.debug_text;disp('XXX -> Total fail');end
            hash= '';
            cent=[0 0 0 0 0 0 0 0];
            return;
        end
        if settings.debug_image
            show(sub,sub_q,a,b,c,d,rad,cent,p,hash);
        end
        
        % Assign cent and complete function
        cent = [a;b;c;d];
    end
end

%% Helper function for settings
function s = check_settings(s,field,default)
if ~isfield(s,field)
    s.(field) = default;
end
end