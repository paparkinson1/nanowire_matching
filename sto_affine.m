%% STO_AFFINE
%   Author : Patrick Parkinson
%   Date   : 19th April 2018
%   Email  : patrick.parkinson@manchester.ac.uk
%   License: CC BY-SA 4.0
%
%   [scale,theta,offset] = sto_affine(aff)
%   scale   : Scaling size
%   theta   : Rotation size
%   offset  : Translation size
%   aff     : affine2D
%
%   A helper function to convert a 2Daffine to scale, theta and offset

function [scale,theta,offset] = sto_affine(aff)

% Calculate scale factor
scale =  sqrt(aff.T(1,1)^2+aff.T(1,2)^2);

% Only calculate theta if required (computationally expensive)
if nargout > 1
    % Basis vectors
    u = [0 1];
    v = [0 0];
    % Calculate others via transforming orthogonal coordinates
    [x, y] = transformPointsForward(aff, u, v);
    dx = x(2) - x(1);
    dy = y(2) - y(1);
    theta =  atan2(dy, dx);
end
% Only calculate offset if required (computationally expensive)
if nargout > 2
    offset= [aff.T(3,1) aff.T(3,2)];
end
end