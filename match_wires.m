%% Match individual wires
%   Author      : Patrick Parkinson
%   Institution : School of Physics & Astronomy, University of Manchester
%   V1 Date     : 16th August 2017
%   Email       : patrick.parkinson@manchester.ac.uk
function matchup=match_wires(original_pos,new_pos,solution,settings)

% Default settings
settings = check_settings(settings,'resolution',200e-6);
settings = check_settings(settings,'match_radius',15);

% Apply transformation to new_pos wires
tx = transformPointsForward(solution, double(new_pos'))'; 

% Clip
o_mask       = logical((original_pos(1,:)<max(tx(1,:)+settings.match_radius)).*...
                       (original_pos(1,:)>min(tx(1,:)-settings.match_radius)).*...
                       (original_pos(2,:)<max(tx(2,:)+settings.match_radius)).*...
                       (original_pos(2,:)>min(tx(2,:)-settings.match_radius)));
original_pos = original_pos(:,o_mask);
origf        = find(o_mask);

if sum(o_mask)==0
    error('No reference points in corresponding area');
end

t_mask       = logical((tx(1,:)<max(original_pos(1,:)+settings.match_radius)).*...
                       (tx(1,:)>min(original_pos(1,:)-settings.match_radius)).*...
                       (tx(2,:)<max(original_pos(2,:)+settings.match_radius)).*...
                       (tx(2,:)>min(original_pos(2,:)+settings.match_radius)));
tx   = tx(:,t_mask);
newf = find(t_mask);

% Get all distances
di = pdist2(original_pos',tx');
% Logical - less than match distance?
dir= di<settings.match_radius;
% Make output array
matchup = zeros(sum(sum(dir)>0),2);
% Set non matched distances to zero
di(dir==0) = NaN;
% Loop over each wire
k = 0;
for i=1:sum(o_mask)
    % Check if any matches
    if any(dir(i,:))
        k=k+1;
        % Find closest match
        [~,b]   = min(di(i,:));
        % Turn this matched wire off
        di(:,b) = NaN;
        dir(:,b)= 0;
        % Record match
        matchup(k,:) = [origf(i),newf(b)];
    end
end
matchup = matchup(1:k,:);
end

%% Helper function for settings
function s = check_settings(s,field,default)
    if ~isfield(s,field)
        s.(field) = default;
    end
end