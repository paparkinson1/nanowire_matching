%%  Match quads
%   Author      : Patrick Parkinson
%   Institution : School of Physics & Astronomy, University of Manchester
%   V1 Date     : 19th July 2017
%   Date        : 19th April 2018
%   Email       : patrick.parkinson@manchester.ac.uk
%   License     : CC BY-SA 4.0
%
%   [matchbook, degeneracy]=match_quads(ref_tree,sample_hash,ref_cents,samp_cents,settings)
%
%       matchbook       :   output matchbook (book of matches)
%       degeneracy      :   output degeneracy (how many matches hit hash)
%       ref_tree        :   input reference quad kd-tree
%       sample_hash     :   input sample hash list
%       ref_cents       :   input reference hash centres
%       samp_cents      :   input sample hash centres
%       settings        :   input settings structure
%
%   This function implements a search algorithm against a kd-tree from a
%   hash list.
function [matchbook, degeneracy]=match_quads(ref_tree,sample_hash,ref_cents,samp_cents,settings)

if ~isa(ref_tree,'KDTreeSearcher')
    error('Function requires a KD tree searcher reference');
end

if ~isa(sample_hash,'double')
    sample_hash = double(sample_hash);
end

if isempty(samp_cents)
    matchbook  = [];
    degeneracy = [];
    return;
end

settings = check_settings(settings,'distance',1);
settings = check_settings(settings,'degeneracy_distance',500);

s = size(sample_hash);
if s(2) == 1
    error('Require unpacked sample hash');
end

% Search KD tree for nearest neighbours
idx = rangesearch(ref_tree,sample_hash,settings.distance);

s   = cellfun(@numel,idx);
f   = find(s);

% Matchbook is given by 16 points - vectors (a,b,c,d) for reference
% and sample image, respectively.
l         = 2*numel(samp_cents(1,:));
matchbook = zeros(sum(s),l);

% Iterate over matches
k=0;
for i=1:numel(f)
    for j =1:s(f(i))
        k=k+1;
        matchbook(k,:) = [samp_cents(f(i),:),ref_cents(idx{f(i)}(j),:)];
    end
end
% Cut down
matchbook = matchbook(1:k,:);

if nargout>1
    % Find the centre of matched quads (reference)
    quadcent   = [sum(matchbook(:,9:2:end),2), sum(matchbook(:,10:2:end),2)]/4;
    % Get the distance between the quad centres
    di=pdist(quadcent);
    % Look for those less than a certain distance apart
    di=squareform(logical(di<settings.degeneracy_distance));       % Choose distance in resolution units
    % Calculate the degeneracy
    degeneracy = sum(di);
    % Sort the degeneracies and matchbook decending
    [degeneracy,order] = sort(degeneracy,'descend');
    matchbook = matchbook(order,:);
end
end

%% Helper function for settings
function s = check_settings(s,field,default)
    if ~isfield(s,field)
        s.(field) = default;
    end
end