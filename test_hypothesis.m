%% test_hypothesis
%   Author : Patrick Parkinson
%   Date   : 19th April 2018
%   Email  : patrick.parkinson@manchester.ac.uk
%   License: CC BY-SA 4.0
%
%   [sout, trans,degs]=test_hypothesis(ref_point_array,sample_point_array,matchbook,degeneracy,settings)
%       sout                :   Score array
%       trans               :   transformation array
%       degs                :   Degeneracy
%       ref_point_array     :   2D point array for reference points
%       sample_point_array  :   2D point array for sample points
%       matchbook           :   Calculated matchbook (from match_quads)
%       degeneracy          :   Degeneracy of matches (from match_quads)
%       settings            :   settings structure
%
%   test_hypothesis determines which quad matches should be tested in
%   detail, and performs a point-by-point matching to generate a score for
%   each match tested. This can be used to approve or reject a given point
%   match.
%   It is noted that this function is non-optimal for large scale use, but
%   is a functional demonstration.
%
function [sout, trans,degs]=test_hypothesis(ref_point_array,sample_point_array,matchbook,degeneracy,settings)

% Settings warning
if nargin<5
    settings = [];
    warning('No settings provided. Using defaults');
end

% Flags
settings = check_settings(settings,'show',false);               % Show matches
settings = check_settings(settings,'show_info',false);          % Show text information

% Settings
settings = check_settings(settings,'match_radius',10);          % In resolution units
settings = check_settings(settings,'FOM','sample');             % Which figure of merit to match
settings = check_settings(settings,'force_unity_scale',false);  % Where the scaling is known
settings = check_settings(settings,'improve_match',true);       % Improve transformation based on later matches
settings = check_settings(settings,'min_degeneracy',0);         % Minimum degeneracy to check

% Get size of sample_point_array
s     = size(sample_point_array);
n_sam = s(2);

% Get size of matchbook
mb_size = size(matchbook);
if mb_size(2) == 16
    error('Unrecognised matchbook size');
end

% Remove low degeneracy matches
if numel(degeneracy)>1
    matchbook = matchbook(degeneracy>settings.min_degeneracy,:);
end
mb_size   = size(matchbook);

if settings.show_info;disp('--> Test Hypothesis');end

if mb_size(1) == 0;error('Overfiltered - no matchbooks available');end

%% Loop over all matches, and calculate FOM
if settings.show_info
    disp(['Testing ',int2str(mb_size(1)),' matches with ', int2str(n_sam),' sample points']);
end

% Create empty arrays
sout   = zeros(mb_size(1),1);
trans  = affine2d.empty;
scale  = sout;
theta  = sout;
offset = zeros(mb_size(1),2);

% Need to operate on doubles
dref_point_array = double(ref_point_array);

% Iterate over each matchbook match
for i =1:mb_size(1)
    % Include a,b,c,d for reflectivity
    [a1, b1, c1,d1,a2, b2,c2,d2] = splitup(matchbook(i,:));
    try
    	trans(i) = fitgeotrans([a1;b1;c1;d1],[a2;b2;c2;d2],'similarity');
    catch err
        if ~strcmp(err.identifier,'images:geotrans:requiredNonCollinearPoints')
            rethrow(err);
        end
    end
    % Calculate transform to apply to sample
    [scale(i),theta(i),offset(i,:)] = sto_affine(trans(i));
    
    % Skip calculation if wildly out
    if and(settings.force_unity_scale,abs(scale(i)-1)>0.1)
        sout(i) = 0;
        continue;
    end
    
    % Apply transform to sample points
    dspa = double(sample_point_array);
    tx = transformPointsForward(trans(i), dspa')';
    
    % Cut reference data down to region around match
    mask = logical((dref_point_array(1,:) < max(tx(1,:))+settings.match_radius) .*...
        (dref_point_array(1,:) > min(tx(1,:))-settings.match_radius) .*...
        (dref_point_array(2,:) < max(tx(2,:))+settings.match_radius) .*...
        (dref_point_array(2,:) > min(tx(2,:))-settings.match_radius));
    dpa = dref_point_array(:,mask);
    
    % Test for overlaps between sample points and reference points
    % predicted by this quad. Iterate over either reference or sample
    % points, looking for a match within the match radius.
    if strcmp(settings.FOM,'sample')
        % Test for sample hitting reference
        dist     = pdist2(tx',dpa')';
        matched = dist < settings.match_radius;
        closest_point = bsxfun(@minus,matched.*dist+(~matched*1e6),min(dist));
    elseif strcmp(settings.FOM,'reference')
        % Test for reference hitting sample
        dist     = pdist2(tx',dpa');
        matched = dist < settings.match_radius;
        closest_point = bsxfun(@minus,matched'.*dist'+(~matched'*1e6),min(dist,[],2)');
    end
    % Choose the matched ones, and re-do fitgeotrans if we have more
    % matched points
    [match_ref,match_samp] = find(~closest_point);
    if and(settings.improve_match,numel(match_ref)>4)
        try
            trans(i) = fitgeotrans(dspa(:,match_samp)',dpa(:,match_ref)','Similarity');
        catch err
            if ~strcmp(err.identifier,'images:geotrans:requiredNonCollinearPoints')
                rethrow(err);
            end
        end
    end
    if strcmp(settings.FOM,'sample')
        % Matched points
        plus_score   = numel(match_samp);
        minus_score  = n_sam-numel(match_samp);
    elseif strcmp(settings.FOM,'reference')
        plus_score   = numel(match_ref);
        minus_score  = sum(mask) - numel(match_ref);
    end
    % Figure of merit - different approaches can be used
    sout(i) = plus_score^3   - (minus_score);
end
sout = (sout - min(sout)+1);

%% Mask down only certain scores.
mask = true(size(sout));
if settings.force_unity_scale
    mask = and(mask,logical(abs(scale-1)<0.05));
end
if settings.show_info
    disp(['Found ',int2str(sum(mask)),' possible matches']);
end
sout = sout(mask);      % Output score
trans= trans(mask);     % Output transformations
if nargout == 3
    ofs  = zeros(sum(mask),2);
    for i =1:sum(mask)
        [~,~,ofs(i,:)] = sto_affine(trans(i));
    end
    offset_distances = squareform(pdist(ofs));
    degs = sum(offset_distances<settings.match_radius*2);
end

%% Show best result
if settings.show
    [ssout,si]=sort(sout,'descend');
    for k =1:min(6,numel(si))
        i     = si(k);
        score = ssout(k);
        % Transform
        tx = transformPointsForward(trans(i), double(sample_point_array'))';
        clf;
        % Plot array
        scatter(tx(1,:),tx(2,:),'rx');
        hold on;
        % Plot reference point array
        scatter(ref_point_array(1,:),ref_point_array(2,:),'b');
        hold off; box on;
        xlim([min(tx(1,:)), max(tx(1,:))]);
        ylim([min(tx(2,:)), max(tx(2,:))]);
        [s, ~, ~] =sto_affine(trans(i));
        title({['Match ',int2str(i),' Score ',num2str(score),' Scale ', num2str(s)]});
        legend('Sample','Reference','location','best');
        waitforbuttonpress;
    end
end
end

%% Helper functions
function [varargout] = splitup(mb)
% Split matchbook
for i=1:nargout
    os = (i-1)*2;
    varargout{i} = mb(1+os:2+os); %#ok<AGROW>
end
end

function s = check_settings(s,field,default)
if ~isfield(s,field)
    s.(field) = default;
end
end