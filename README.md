# nanowire_matching

- Author      : Patrick Parkinson
- Institution : School of Physics & Astronomy, University of Manchester
- Date        : 16th April 2018
- Email       : patrick.parkinson@manchester.ac.uk
- License     : CC BY-SA 4.0

Project for matching nanowire positions via multiple methodologies, based 
on the astrometry.net approach by Lang et al 
[The Astronomical Journal, v139, p1782](https://arxiv.org/abs/0910.2233 "arxiv.org/abs/0910.2233").

Extended and transferred to work with point array for multimodal matching 
of nanowires by [Patrick Parkinson](patrick.parkinson@manchester.ac.uk).

This code is provided as a reference to the approach used, and may be 
freely adapted for other uses as per the License CC BY-SA 4.0 (see LICENSE 
file).

This code accompanies a research paper, [*"Modal refractive index measurement in nanowire lasers—a correlative approach"*, 
Patrick Parkinson et al 2018 **Nano Futures**, 2, 035004](https://doi.org/10.1088/2399-1984/aad0c6).

It is written and tested for *MATLAB 2017a (Windows 64bit)*.

## General approach

This code is provided as a reference only, and will not likely work for a 
given application without careful tuning of the *settings*
parameter. This parameter is described within the functions provided here.

1. Produce a 2D array in MATLAB representing positions of object in a 
reference field.
2. Feed this to quads.m to produce hashs, centres, and a search tree: 
*[hashs,cents, hash_tree]=quads(pos_array,settings)*
3. Do this for the sample points, producing hash and centres.
4. Run match_quads.m to produce a list of matches from the tree and the 
sample hashs: *[matchbook, degeneracy]=match_quads(ref_tree,sample_hash,
ref_cents,samp_cents,settings)*
5. Test the matches to find the best match: *[sout, trans,degs]=test_
hypothesis(ref_point_array,sample_point_array,matchbook,degeneracy,
settings)*
6. Show the best/selected transformation: *tr=show_transformation(
sample_points,reference_points, scores, affines,settings)*